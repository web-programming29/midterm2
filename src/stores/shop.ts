import { ref } from "vue";
import { defineStore } from "pinia";
import type Shop from "@/stores/types/shop";

export const useShopStore = defineStore("shopping", () => {
  const myShop = ref<Shop[]>([]);
  const sum = ref(0);

  function add(id: number, name: string, price: number) {
    myShop.value.push({ id, name, price });
    sum.value += price;
  }

  return { myShop, sum, add };
});
