export default interface Shop {
  id: number;
  name: string;
  price: number;
}
